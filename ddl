CREATE TABLE ads_account
(
  account_id bigint NOT NULL,
  currency_code text,
  created_at timestamp,
  timestamp_timezone text,
  data_snapshot_timestamp timestamp
);


COMMENT ON TABLE ads_account IS E'Google Ads account details';


COMMENT ON COLUMN ads_account.account_id IS E'Google Ads account ID. Static value';


COMMENT ON COLUMN ads_account.currency_code IS E'Currency code. Static';


COMMENT ON COLUMN ads_account.created_at IS E'Timestamp when Google Ads account was created ';


COMMENT ON COLUMN ads_account.data_snapshot_timestamp IS E'Date of a historical data snapshot; it allows to show historical values of metrics for reported time';

CREATE TABLE ads_campaign
(
  account_id bigint NOT NULL,
  name text,
  status text,
  budget bigint,
  campaign_id bigint NOT NULL,
  advertising_channel_type text,
  data_snapshot_timestamp timestamp,
  timestamp_timezone text,
  campaign_start_date timestamp,
  campaign_end_date timestamp
);


COMMENT ON TABLE ads_campaign IS E'Data that describes Google ads campaign';


COMMENT ON COLUMN ads_campaign.account_id IS E'Google Ads account ID. Static value';


COMMENT ON COLUMN ads_campaign.name IS E'Campaign name. It can be edited in settings, not a static value';


COMMENT ON COLUMN ads_campaign.status IS E'Campaign status. Not static. Possible values: Paused, Pending, Ended, Removed, Under review, Eligible, Eligible (limited), Not eligible, Disapproved: ';


COMMENT ON COLUMN ads_campaign.budget IS E'Campaign budget. Not static ';


COMMENT ON COLUMN ads_campaign.campaign_id IS E'Campaign ID. Static field';


COMMENT ON COLUMN ads_campaign.advertising_channel_type IS E'Field describing the various advertising channel types like: unspecified, unknown, search, display, video. Not static   ';


COMMENT ON COLUMN ads_campaign.data_snapshot_timestamp IS E'Date of a historical data snapshot; it allows to show historical values of metrics for reported time';


COMMENT ON COLUMN ads_campaign.campaign_end_date IS E'End date can be earlier than today\'s date as Client can set it up in campaign settings';

CREATE TABLE ads_group_metrics
(
  campaign_id bigint NOT NULL,
  group_id bigint NOT NULL,
  type text,
  effective_target_roas bigint,
  effective_target_roas_source text,
  cpc_bid_micros bigint,
  all_conversions bigint,
  all_conversions_value bigint,
  all_conversions_value_per_cost bigint,
  clicks bigint,
  cost_micros bigint,
  ctr bigint,
  impressions bigint,
  average_cpc bigint,
  cost_per_conversion bigint,
  conversions_value bigint,
  interaction_rate double precision,
  interactions bigint,
  search_top_impression_share bigint,
  search_impression_share double precision,
  search_click_share double precision,
  all_conversions_value_per_cost_calculated bigint,
  data_snapshot_timestamp timestamp,
  timestamp_timezone text,
  day_of_week text,
  keyword_match_type text,
  keyword_text text
);


COMMENT ON TABLE ads_group_metrics IS E'Data that describes ads group details and metrics';


COMMENT ON COLUMN ads_group_metrics.campaign_id IS E'Campaign ID. Static field';


COMMENT ON COLUMN ads_group_metrics.group_id IS E'Group ID. Static field ';


COMMENT ON COLUMN ads_group_metrics.type IS E'Ads group type. Not statis';


COMMENT ON COLUMN ads_group_metrics.effective_target_roas IS E'Polska definicja jest bardziej obrazowa chyba: `Docelowy ROAS pozwala ustalać stawki pod kątem docelowego zwrotu z nakładów na reklamę (ROAS) `. Not static';


COMMENT ON COLUMN ads_group_metrics.effective_target_roas_source IS E'Source of the effective target ROAS. Not static';


COMMENT ON COLUMN ads_group_metrics.cpc_bid_micros IS E'The maximum CPC (cost-per-click) bid. Not static';


COMMENT ON COLUMN ads_group_metrics.all_conversions IS E'Total number of conversions. Not static';


COMMENT ON COLUMN ads_group_metrics.all_conversions_value IS E'The value of all conversions.Not static';


COMMENT ON COLUMN ads_group_metrics.all_conversions_value_per_cost IS E'The value of all conversions divided by the total cost of ad interactions (such as clicks for text ads or views for video ads). Not static';


COMMENT ON COLUMN ads_group_metrics.clicks IS E'Number of clicks. Not static';


COMMENT ON COLUMN ads_group_metrics.cost_micros IS E'The sum of your cost-per-click (CPC) and cost-per-thousand impressions (CPM) costs during this period. Not static';


COMMENT ON COLUMN ads_group_metrics.ctr IS E'The number of clicks your ad receives (Clicks) divided by the number of times your ad is shown (Impressions). Not static';


COMMENT ON COLUMN ads_group_metrics.impressions IS E'Count of how often your ad has appeared on a search results page or website on the Google Network. Not static';


COMMENT ON COLUMN ads_group_metrics.average_cpc IS E'Total cost of all clicks divided by the total number of clicks received. Not static';


COMMENT ON COLUMN ads_group_metrics.cost_per_conversion IS E'The cost of ad interactions divided by conversions. Not static';


COMMENT ON COLUMN ads_group_metrics.conversions_value IS E'The value of conversions. This only includes conversion actions which include_in_conversions_metric attribute is set to true. Not static';


COMMENT ON COLUMN ads_group_metrics.interaction_rate IS E'How often people interact with your ad after it is shown to them. This is the number of interactions divided by the number of times your ad is shown.';


COMMENT ON COLUMN ads_group_metrics.interactions IS E'\nThe number of interactions. An interaction is the main user action associated with an ad format-clicks for text and shopping ads, views for video ads, and so on.\n';


COMMENT ON COLUMN ads_group_metrics.search_top_impression_share IS E'The impressions you\'ve received in the top location (anywhere above the organic search results) compared to the estimated number of impressions you were eligible to receive in the top location. Note: Search top impression share is reported in the range of 0.1 to 1. Any value below 0.1 is reported as 0.0999.';


COMMENT ON COLUMN ads_group_metrics.search_impression_share IS E'The impressions you\'ve received on the Search Network divided by the estimated number of impressions you were eligible to receive. Note: Search impression share is reported in the range of 0.1 to 1. Any value below 0.1 is reported as 0.0999.';


COMMENT ON COLUMN ads_group_metrics.search_click_share IS E'The number of clicks you\'ve received on the Search Network divided by the estimated number of clicks you were eligible to receive. Note: Search click share is reported in the range of 0.1 to 1. Any value below 0.1 is reported as 0.0999.';


COMMENT ON COLUMN ads_group_metrics.data_snapshot_timestamp IS E'Date of a historical data snapshot; it allows to show historical values of metrics for reported time';


COMMENT ON COLUMN ads_group_metrics.keyword_match_type IS E' Match type for the Keyword  possible values  exact, phrase, broad';

CREATE TABLE geo_details
(
  group_id bigint NOT NULL,
  geo_target_most_specific_location text,
  geo_target_postal_code text,
  geo_target_county text,
  geo_target_city text
);


COMMENT ON TABLE geo_details IS E'Table holds geo details';

CREATE TABLE product_metrics
(
  product_item_id bigint,
  product_bidding_category_level1 text,
  product_bidding_category_level2 text,
  product_brand text,
  product_bidding_category_level3 text,
  product_bidding_category_level4 text,
  product_bidding_category_level5 text,
  product_channel text,
  product_condition text,
  product_country text,
  product_custom_attribute0 text,
  product_custom_attribute1 text,
  product_custom_attribute2 text,
  product_custom_attribute3 text,
  product_custom_attribute4 text,
  product_store_id bigint,
  product_title text,
  product_type_l1 text,
  product_type_l2 text,
  product_type_l3 text,
  product_type_l4 text,
  product_type_l5 text,
  group_id bigint NOT NULL,
  GTIN_code text
);


COMMENT ON TABLE product_metrics IS E'Table holds data about product';


COMMENT ON COLUMN product_metrics.product_channel IS E'possible values: local, online, unknown, unspecified		';


COMMENT ON COLUMN product_metrics.product_condition IS E'possible values: new, refurbished, unknown, unspecified,\nused';


COMMENT ON COLUMN product_metrics.product_title IS E'\nTitle of the product.\n';


COMMENT ON COLUMN product_metrics.GTIN_code IS E'Product code';

CREATE TABLE sempai_customer
(
  account_id bigint NOT NULL,
  industry text,
  sempai_sign_up_date timestamp,
  sempai_customer_internal_id bigint,
  data_snapshot_timestamp bigint,
  timestamp_timezone text
);


COMMENT ON TABLE sempai_customer IS E'Internal data about Sempai\'s Customers';


COMMENT ON COLUMN sempai_customer.account_id IS E'Google Ads account ID. Static value';


COMMENT ON COLUMN sempai_customer.industry IS E'Sempai\'s customer industry. Value from CRM tool. Static value ';


COMMENT ON COLUMN sempai_customer.sempai_sign_up_date IS E'Date when customer started cooperation with Sempai. Static value.';


COMMENT ON COLUMN sempai_customer.sempai_customer_internal_id IS E'Insternal ID of Sempai Customer; there might be situation wehere one customer has multiple Google Ads accounts';


COMMENT ON COLUMN sempai_customer.data_snapshot_timestamp IS E'Date of a historical data snapshot; it allows to show historical values of metrics for reported time';

